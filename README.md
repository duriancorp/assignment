# Assignment
## Web Application: Income and Expense Recording

Please create your git repo for make this web app with React and Node.js + any Database (such as MySQL, MongoDB, etc)

## Feature
### Authentication
- Register new user
- Login
### Income
- Record => item name, amount of money, date
- Add Income
- Edit Income
- Delete Income
### Expense
- Record => item name, amount of money, date
- Add Expense
- Edit Expense
- Delete Expense

## Frontend

### Login Page
login with username and password

### Signup Page
signup with username and password

### Daily Page
show daily income and expense  (user can choose date to view, default to today date)
click on item to edit
show summary total incomse/expense

### Create Income Page
create new income record

### Edit Income Page
edit income record
delete record button

### Create Expense Page
create new expense record

### Edit Expnse Page
edit expense record
delete record button

## Backend
make API for supporting frontend with javascript or typescript that run on Node.js and design database for this app (You can use any framework and any database)

# Submission material
Git repository url for your source code
Enitity Relationship Diagram (database design)
